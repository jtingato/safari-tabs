//
//  math.swift
//  TabSelector
//
//  Created by John Ingato on 2/12/16.
//  Copyright © 2016 John Ingato. All rights reserved.
//

import Foundation

// Functions which are already present in NTBrowser's Utility class

func normalize(min: Double, max: Double, value: Double) -> Double {
	return ((value - min) / (max - min)) * 100.0
}

func lerp(min: Double, max: Double, decPercentage: Double) -> Double {
	return (1 - decPercentage) * min + (decPercentage * max)
}

func slerp(min: Double, max: Double, decPercentage: Double) -> Double {
	return min + (decPercentage * (min-max))
}
