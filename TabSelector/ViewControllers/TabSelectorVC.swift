//
//  TabSelectorVC.swift
//
//  Created by John T Ingato on 02/02/16.
//  Copyright © 2016 Ntent,Inc. All rights reserved.
//

import UIKit
import CoreData

class TabSelectorVC: UIViewController {
	
	@IBOutlet var CollectionView: UICollectionView!
	
	// Debug flag
	var dataWasInitialized = false
	let dataSetShouldGrow = false
	
	let imageCache = NSCache()
	var fetchedResultsController: NSFetchedResultsController?
	let context = CoreDataStack.sharedInstance.managedObjectContext!

	
	let TOP = CGFloat(0.0)
	
	var tabSize: CGFloat = 0
    var tabSpacing: CGFloat = -180.0
    let tabCell = "roundedCornerTab"
	
// MARK: - Lifecycle Methods
	override func viewDidLoad() {
		
		initializeFetchController()
		try! fetchedResultsController!.performFetch()
		if let path = NSBundle.mainBundle().pathForResource("InitialData", ofType: "plist") {
			if let data = NSArray(contentsOfFile:path) {
				if dataSetShouldGrow == false {
					if fetchedResultsController?.fetchedObjects?.count == 0 {
						initializeCoreData(data)
					}
				} else {
					initializeCoreData(data)
				}
			}
		}
	}
	
	override func viewDidLayoutSubviews() {
		let size = self.view.frame.size
		let overlapPercentage = CGFloat(UIApplication.sharedApplication().statusBarOrientation == .Portrait ? 0.65 : 0.25)
		
		tabSize = CGFloat(UIApplication.sharedApplication().statusBarOrientation == .Portrait ? min(size.width, size.height) : max(size.width, size.height))
		tabSpacing = -(tabSize * overlapPercentage)
		
	}
	
// MARK: - Initialization
	func initializeFetchController() {
		guard fetchedResultsController == nil else {
			return
		}
		// Build fetch request
		let fetchRequest = NSFetchRequest(entityName: "Tab")
		fetchRequest.fetchLimit = 100
		fetchRequest.fetchBatchSize = 15
		fetchRequest.includesPendingChanges = true
		
		fetchRequest.sortDescriptors = [] // [NSSortDescriptor(key: "date", ascending: false)]
			
		fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
		fetchedResultsController!.delegate = self;
		try! fetchedResultsController!.performFetch()
	}
	
	func initializeCoreData(data: NSArray) {
		for x in 0 ..< data.count {
			let historyItem = NSEntityDescription.insertNewObjectForEntityForName("History", inManagedObjectContext: context) as! History
			historyItem.title 	= (data[x] as! NSDictionary)["title"] as? String
			historyItem.url 	= (data[x] as! NSDictionary)["url"] as? String
			historyItem.date 	= NSDate()
			
			let thisTab = NSEntityDescription.insertNewObjectForEntityForName("Tab", inManagedObjectContext: context) as! Tab
			if let thisImage = UIImage(named: historyItem.title!) {
				thisTab.title 	= historyItem.title
				thisTab.date 	= NSDate()
				thisTab.preview = UIImageJPEGRepresentation(thisImage, 0.50)
				imageCache.setObject(thisTab.preview!, forKey: historyItem)
			}
			historyItem.tab = thisTab
		}
		
		 if let _ = try? context.save() {
			print("Initial data Saved")
		}

	}

}

// MARK: - CollectionView Datasource and Delegates
extension TabSelectorVC: UICollectionViewDataSource {
	
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return fetchedResultsController!.fetchedObjects!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCellWithReuseIdentifier(tabCell, forIndexPath: indexPath) as! CornerCollectionViewCell
		let tab = fetchedResultsController!.objectAtIndexPath(indexPath) as! Tab
		let image = UIImage(data: tab.preview!)
                
        cell.title = tab.title //"TAB \(indexPath.row)"
		cell.image = image
        return cell
    }
	
	
}

extension TabSelectorVC: UICollectionViewDelegate {
	func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
		
		guard cell.layer.shadowPath == nil else {
			return
		}
		
		let myCell = cell as! CornerCollectionViewCell
		myCell.backingView.layer.cornerRadius = 4
		myCell.backingView.layer.masksToBounds = true
		
		myCell.layer.shadowPath = UIBezierPath(roundedRect: myCell.layer.bounds, cornerRadius: myCell.backingView.layer.cornerRadius).CGPath
		myCell.layer.shadowRadius = 15
		myCell.layer.shadowColor = UIColor.blackColor().CGColor
		myCell.layer.shadowOpacity = 0.55
		myCell.layer.shadowOffset = CGSize(width: 0, height: -20)
		myCell.layer.masksToBounds = false
		myCell.layer.shouldRasterize = true
		myCell.layer.anchorPoint = CGPoint(x: 0.5, y: TOP)
	}
	
	func collectionView(collectionView: UICollectionView, didEndDisplayingCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
		
		if let myCell = cell as? CornerCollectionViewCell {
			printCellInfo(myCell,index: indexPath.row)
		}
	}
	
	func printCellInfo(cell:CornerCollectionViewCell, index:Int) {
		let position = String(format: "(%.0f : %.0f)", cell.frame.origin.x, cell.frame.origin.y)
		print("Ending display of index \(index) - \(cell.titleText.text) -- \(position)")
	}
}


// MARK: - FlowLayout Delegates
extension TabSelectorVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: tabSize,height: tabSize)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: NSInteger) -> CGFloat {
        return tabSpacing
    }
}

// MARK: - FetchedResultsControllerDelegate
extension TabSelectorVC: NSFetchedResultsControllerDelegate {
	
	func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
		
	}
 
	func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
		switch(type) {
		case .Insert:
			// Add new tab to collection
			break
		case .Delete:
			// Delete selected tab
			break
		case .Update:
			break
		case .Move:
			// Not implementing manual sort
			break
		}
	}
 
	func controllerWillChangeContent(controller: NSFetchedResultsController) {
		
	}
 
	func controllerDidChangeContent(controller: NSFetchedResultsController) {
		
	}
}
