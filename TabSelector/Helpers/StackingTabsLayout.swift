//
//  StackingTabsLayout.swift
//  StackingTabsLayout
//
//  Created by John T Ingato on 02/08/16.
//  Copyright © 2016 Ntent Inc. All rights reserved.
//

import UIKit

class StackingTabsLayout: UICollectionViewFlowLayout {
    
    var firstItemTransform: CGFloat?
	let stackSpacing = CGFloat(120)
    
    override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let items = NSArray (array: super.layoutAttributesForElementsInRect(rect)!, copyItems: true)
        var headerAttributes: UICollectionViewLayoutAttributes?
        
        items.enumerateObjectsUsingBlock { (object, index, stop) -> Void in
            let attributes = object as! UICollectionViewLayoutAttributes
            
            if attributes.representedElementKind == UICollectionElementKindSectionHeader {
                headerAttributes = attributes
            }
            else {
                self.updateCellAttributes(attributes, headerAttributes: headerAttributes)
            }
        }
        return items as? [UICollectionViewLayoutAttributes]
    }
    
    func updateCellAttributes(attributes: UICollectionViewLayoutAttributes, headerAttributes: UICollectionViewLayoutAttributes?) {
		let index = CGFloat(attributes.indexPath.row)
        let minY = CGRectGetMinY(collectionView!.bounds) + collectionView!.contentInset.top + (index * stackSpacing)
        let maxY = attributes.frame.origin.y
		
        let finalY = max(minY, maxY)
        var origin = attributes.frame.origin
        
        origin.y = finalY
        attributes.frame = CGRect(origin: origin, size: attributes.frame.size)
        attributes.zIndex = attributes.indexPath.row
		//print(String(format:"Item %.0f = (y: %.0f to %.0f)   (content: %.0f)  (frame: %.0f)\n", index, attributes.frame.origin.y, attributes.frame.origin.y + attributes.frame.size.height, collectionView!.contentSize.height, collectionView!.frame.size.height) )
    }
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        return true
    }
}
