//
//  NormalTabLayout.swift
//  TabSelector
//
//  Created by John Ingato on 2/10/16.
//  Copyright © 2016 John Ingato. All rights reserved.
//

import UIKit

class SafariStyleLayout: UICollectionViewFlowLayout {
	
	let tabScale = CGFloat(0.95)
	let tabTilt = Double(0)
	var offset = CGFloat(0)
	
	// The BOUNDS of the collectionView's contentView. Represents the visible 
	// frame of the contentView relative to the content views coordinates
	var virtualWindow = CGRectZero
	
	// Represents a vertical position equal to the halfway point between the
	// top and bottom of the visible portion of the scrolling content
	var virtualHorizon = CGFloat(0)
	
	override func layoutAttributesForElementsInRect(rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
		let items = NSArray (array: super.layoutAttributesForElementsInRect(rect)!, copyItems: true)
		var headerAttributes: UICollectionViewLayoutAttributes?
		
		virtualWindow = (collectionView?.bounds)!
		virtualHorizon = CGRectGetMidY(virtualWindow)
		//print(String(format: "😷 Visible Window: (%.2f - %.2f)", CGRectGetMinY(virtualWindow), CGRectGetMaxY(virtualWindow) ))
		
		items.enumerateObjectsUsingBlock { (object, index, stop) -> Void in
			let attributes = object as! UICollectionViewLayoutAttributes
			if attributes.representedElementKind == UICollectionElementKindSectionHeader {
				headerAttributes = attributes
			}
			else {
				self.updateCellAttributes(attributes, headerAttributes: headerAttributes)
			}
		}
		return items as? [UICollectionViewLayoutAttributes]
	}
	
	func updateCellAttributes(attributes: UICollectionViewLayoutAttributes, headerAttributes: UICollectionViewLayoutAttributes?) {
		let rotationalFactor = normalizeCG(CGRectGetMinY(virtualWindow), max: CGRectGetMaxY(virtualWindow), value: attributes.frame.origin.y)
		print("😷 \(rotationalFactor)")
		attributes.zIndex = attributes.indexPath.row
		attributes.transform3D = transformForCell(attributes, withCorrectionalOffset: attributes.frame.width / 2, andRotationFactor: rotationalFactor)
	}
	
	override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
		return true
	}
	
	
	func transformForCell(attributes: UICollectionViewLayoutAttributes, withCorrectionalOffset offset:CGFloat, andRotationFactor factor:Double) -> CATransform3D {
		let finalTilt = clamp(tabTilt + (60 * factor), minVal: 20, maxVal: 80)
		print(String(format:"Item %d is at %.0f%% - Tilt: %.0f", attributes.indexPath.row, factor * 100, finalTilt))
		
		var currentTransform = attributes.transform3D
		currentTransform.m34 = -1.0/600
		currentTransform = CATransform3DTranslate(currentTransform, 0, -offset, 0.0)
		currentTransform = CATransform3DRotate(currentTransform, CGFloat(finalTilt * M_PI) / -180.0, 1.0, 0.0, 0.0);
		currentTransform = CATransform3DScale(currentTransform, tabScale, tabScale, 1.0)
		
		return currentTransform
	}
	
	func tiltFactor() -> CGFloat {
		return 0
	}
}


extension SafariStyleLayout {
	func normalizeCG(min: CGFloat, max: CGFloat, value: CGFloat) -> Double {
		return Double((value - min) / (max - min))
	}
	
	func clamp<T: Comparable> (value: T, minVal: T, maxVal: T) -> T {
		return min(max(value, minVal), maxVal)
	}
}

