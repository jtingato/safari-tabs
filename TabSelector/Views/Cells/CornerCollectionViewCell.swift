//
//  CornerCollectionViewCell.swift
//  StickyCollectionView
//
//  Created by Bogdan Matveev on 02/02/16.
//  Copyright © 2016 Bogdan Matveev. All rights reserved.
//

import UIKit

class CornerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var backingView: UIView!
	@IBOutlet weak var screenshot: UIImageView!
	@IBOutlet weak var closeButton: UIButton!
    
    var title: String? {
        didSet {
            titleText.text = title
        }
    }
    
    var color: UIColor? {
        didSet {
            backingView.backgroundColor = color
        }
    }
	
	var image: UIImage? {
		didSet {
			screenshot.image = image
		}
	}
	
	@IBAction func closeButtonPressed(sender: UIButton) {
		if sender.superview!.superview!.superview! == self {
			
		}
		print("Closing the Tab")
	}
	
}
