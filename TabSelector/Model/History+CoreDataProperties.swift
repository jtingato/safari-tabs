//
//  History+CoreDataProperties.swift
//  TabSelector
//
//  Created by John Ingato on 2/11/16.
//  Copyright © 2016 John Ingato. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension History {

    @NSManaged var title: String?
    @NSManaged var url: String?
    @NSManaged var date: NSDate?
    @NSManaged var tab: Tab?

}
